import sys
import math
import numpy
import matplotlib.pyplot as p
import scipy.cluster.vq
import common_txrx as common
from graphs import *
from numpy import linalg as LA

import hamming_db
import channel_coding as cc

class Receiver:
    def __init__(self, carrier_freq, samplerate, spb):
        '''
        The physical-layer receive function, which processes the
        received samples by detecting the preamble and then
        demodulating the samples from the start of the preamble
        sequence. Returns the sequence of received bits (after
        demapping)
        '''
        self.fc = carrier_freq
        self.samplerate = samplerate
        self.spb = spb
        print 'Receiver: '

    def detect_threshold(self, demod_samples):
        '''
        Returns representative sample values for bit 0, 1 and the threshold.
        Use kmeans clustering with the demodulated samples
        '''
        # fill in your implementation
        one_cluster_value = 1
        zero_cluster_value = 0
        diff_threshold = .001
        while True:
            one_sum = 0
            one_count = 0
            zero_sum = 0
            zero_count = 0
            for i in range(len(demod_samples)):
                one_distance = abs(one_cluster_value - demod_samples[i])
                zero_distance = abs(zero_cluster_value - demod_samples[i])
                if one_distance < zero_distance:
                    one_sum += demod_samples[i]
                    one_count += 1
                else:
                    zero_sum += demod_samples[i]
                    zero_count += 1

            if one_count > 0:
                one_mean = float(one_sum) / one_count
            else:
                one_mean = 0
            if zero_count > 0:
                zero_mean = float(zero_sum) / zero_count
            else:
                zero_mean = 0

            one_diff = abs(one_cluster_value - one_mean)
            zero_diff = abs(zero_cluster_value - zero_mean)
            if(zero_diff < diff_threshold and one_diff < diff_threshold):
                return (one_mean, zero_mean, float(one_mean + zero_mean) / 2)
            else:
                one_cluster_value = one_mean
                zero_cluster_value = zero_mean

    def find_energy_offset(self, demod_samples, thresh, one):
        energy_offset = 0
        demod_sample_length = len(demod_samples)
        while True:
            if self.spb > 3:
                curSamples  = demod_samples[energy_offset + self.spb/4 : energy_offset + 3 * self.spb/4]
            else:
                curSamples = demod_samples[energy_offset:energy_offset + self.spb]
            curSampleMean = numpy.mean(curSamples)
            if curSampleMean > (one + thresh) / 2:
                print "Energy Offset ", energy_offset
                return energy_offset
            else:
                energy_offset += 1
            if energy_offset >= demod_sample_length:
                return -1

    def find_preamble_offset(self, demod_samples, thresh, one, offset):
        pre_offset = 0
        curMaxValue = 0

        preamble_as_samples = numpy.array(common.bits_as_samples_helper(common.preamble, one, self.spb))
        preamble_length = len(preamble_as_samples)
        max_index = len(demod_samples) - preamble_length
        heuristic_max_index = offset + 3*len(preamble_as_samples)
        if max_index > heuristic_max_index:
            max_index = heuristic_max_index
        for i in range(offset,max_index):
            curArray = demod_samples[i: i + preamble_length]
            # Perform cross correlation
            curValue = numpy.dot(preamble_as_samples, curArray)
            curValue = curValue / numpy.linalg.norm(curArray)

            # If it's the highest value so far, mark it
            if curValue > curMaxValue:
                curMaxValue = curValue
                pre_offset = i

        '''
        [pre_offset] is the additional amount of offset starting from [offset],
        (not a absolute index reference by [0]).
        Note that the final return value is [offset + pre_offset]
        '''
        print "Preamble Offset (relative to energy_offset) ", pre_offset - offset
        print "Global Preamble Offset ", pre_offset
        return pre_offset


    def detect_preamble(self, demod_samples, thresh, one):
        '''
        Find the sample corresp. to the first reliable bit "1"; this step
        is crucial to a proper and correct synchronization w/ the xmitter.
        '''

        '''
        First, find the first sample index where you detect energy based on the
        moving average method described in the milestone 2 description.
        '''
        # Fill in your implementation of the high-energy check procedure
        energy_offset = self.find_energy_offset(demod_samples, thresh, one)

        # Find the sample corresp. to the first reliable bit "1"; this step
        # is crucial to a proper and correct synchronization w/ the xmitter.
        offset = energy_offset
        if offset < 0:
            print '*** ERROR: Could not detect any ones (so no preamble). ***'
            print '\tIncrease volume / turn on mic?'
            print '\tOr is there some other synchronization bug? ***'
            sys.exit(1)

        '''
        Then, starting from the demod_samples[offset], find the sample index where
        the cross-correlation between the signal samples and the preamble
        samples is the highest.
        '''
        # Fill in your implementation of the cross-correlation check procedure
        return self.find_preamble_offset(demod_samples, thresh, one, offset)

    def demap_and_check(self, demod_samples, barker_start):
        '''
        Demap the demod_samples (starting from [preamble_start]) into bits.
        1. Calculate the average values of midpoints of each [spb] samples
           and match it with the known preamble bit values.
        2. Use the average values and bit values of the preamble samples from (1)
           to calculate the new [thresh], [one], [zero]
        3. Demap the average values from (1) with the new three values from (2)
        4. Check whether the first [preamble_length] bits of (3) are equal to
           the preamble. If it is proceed, if not print an error message and
           terminate the program.
        Output is the array of data_bits (bits without preamble)
        '''
        result_arr = common.compute_threshold(demod_samples, barker_start, self.spb)
        if result_arr is None or common.valid_preamble(demod_samples, self.spb, barker_start, result_arr[2]) == False:
            print '*** ERROR: Could not detect the preamble. ***'
            print '\tIncrease volume / turn on mic?'
            print '\tOr is there some other synchronization bug? ***'
            sys.exit(1)
        threshold_value = result_arr[2]
        return common.find_databits(demod_samples, barker_start, self.spb, threshold_value)

    def demodulate(self, samples):
        '''
        Perform quadrature modulation.
        Return the demodulated samples.
        '''
        listLength = len(samples)
        sin_demod_samples = numpy.zeros(listLength)
        cos_demod_samples = numpy.zeros(listLength)
        for n in range(listLength):
             sin_demod_samples[n] = math.sin(float(2 * math.pi * n * self.fc) / self.samplerate) * samples[n]
             cos_demod_samples[n] = math.cos(float(2 * math.pi * n * self.fc) / self.samplerate) * samples[n]
        sin_lpfilter = common.lpfilter(sin_demod_samples, (math.pi * self.fc/self.samplerate))
        cos_lpfilter = common.lpfilter(cos_demod_samples, (math.pi * self.fc/self.samplerate))

        demod_samples = numpy.zeros(listLength)
        for n in range(listLength):
          demod_samples[n] = math.sqrt(sin_lpfilter[n] * sin_lpfilter[n] + cos_lpfilter[n] * cos_lpfilter[n])
        return demod_samples

    def decode(self, recd_bits):
        return cc.get_databits(recd_bits)
