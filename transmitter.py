import math
import common_txrx as common
import numpy

import hamming_db
import channel_coding as cc
 
# Eventually test voltage parameter != 1 and see what happens
# Potential errors: add_preamble (Piazza question)
# Definite errors: modulate -> common.lpfilter

class Transmitter:
    def __init__(self, carrier_freq, samplerate, one, spb, silence, cc_len):
        self.fc = carrier_freq  # in cycles per sec, i.e., Hz
        self.samplerate = samplerate
        self.one = one
        self.spb = spb
        self.silence = silence
        self.cc_len = cc_len
        print 'Transmitter: '

    def add_preamble(self, databits):
        '''
        Prepend the array of source bits with silence bits and preamble bits
        The recommended preamble bits is 
        [1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1]
        The output should be the concatenation of arrays of
            [silence bits], [preamble bits], and [databits]
        '''
        silence_array = numpy.zeros(self.silence)
        preamble_array = numpy.array(common.preamble)
        databits_with_preamble = numpy.concatenate([silence_array, preamble_array, databits])
        return databits_with_preamble

    def bits_to_samples(self, databits_with_preamble):
        '''
        Convert each bits into [spb] samples. 
        Sample values for bit '1', '0' should be [one], 0 respectively.
        Output should be an array of samples.
        '''
        samples = numpy.zeros(len(databits_with_preamble) * self.spb)
        for i in range(len(databits_with_preamble)):
            volt = self.one if databits_with_preamble[i] == 1 else 0
            for j in range(self.spb):
                samples[self.spb * i + j] = volt
                if databits_with_preamble[i] != 1 and databits_with_preamble[i] != 0:
                    raise Exception('Databits not ints as expected')
        assert len(samples) == len(databits_with_preamble) * self.spb
        return samples
        
    def modulate(self, samples):
        '''
        Multiply samples by a local sinusoid carrier of the same length.
        Return the multiplied result.
        '''
        print '\tNumber of samples being sent:', len(samples)
        mod_samples = numpy.array(samples)
        for n in range(len(samples)):
            mod_samples[n] = samples[n] * math.cos(float(2 * math.pi * self.fc * n) / self.samplerate)
        mod_samples = common.lpfilter(mod_samples, float(4 * math.pi * self.fc) / self.samplerate)
        return mod_samples

    def encode(self, databits, cc_len):
        '''
        Wrapper function for milestone 2. No need to touch it        
        '''
        return cc.get_frame(databits, cc_len)