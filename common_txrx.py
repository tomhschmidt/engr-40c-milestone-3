import numpy
import math
import operator

import binascii
# Methods common to both the transmitter and receiver.

preamble = [1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1]

def average_middle_window(ext_preamble, spb):
    average_array = numpy.zeros(len(ext_preamble) / spb)
    for i in range(len(average_array)):
        mid_window = create_middle_window(ext_preamble, spb, i)
        average_array[i] = numpy.mean(mid_window)
    return average_array

def bits_as_samples_helper(bits, one, spb):
    samples = []
    for i in range(len(bits)):
        sample = 0
        if(bits[i] == 1):
            sample = one
        else:
            sample = 0
        for j in range(spb):
            samples.append(sample)
    return samples

def create_h_array(L, omega_cut):
    h_array = numpy.zeros(2*L + 1)
    index = 0
    for i in range(-L, L, 1):
        if i == 0:
            h_array[index] = float(omega_cut) / math.pi
        else:
            h_array[index] = float(math.sin(omega_cut * i)) / (math.pi * i)
        index += 1
    return h_array

def create_x_array(L, samples, length, n):
    if n-L >= 0 and n + 2*L < length:
        x_array = samples[n-L:n-L + 2*L+1]
    else:
        x_array = numpy.zeros(2*L + 1)
        index = 0
        for k in range(-L, L, 1):
            if n-k >= 0 and n-k < length:
                x_array[index] = samples[n-k]
            else:
                x_array[index] = 0
            index += 1
    assert len(x_array) == 2*L + 1
    assert x_array[L] == samples[n]
    return x_array


def lpfilter(samples_in, omega_cut):
    '''
    A low-pass filter of frequency omega_cut.
    '''
    # set the filter unit sample response
    # convolve unit sample response with input samples
    # fill in your implementation
    samples_length = len(samples_in)
    samples_out = numpy.zeros(samples_length)
    L = 50
    h_array = create_h_array(L, omega_cut)
    for n in range(len(samples_out)):
        x_array = create_x_array(L, samples_in, samples_length, n)
        samples_out[n] = numpy.dot(x_array, h_array)
    return samples_out

# Demap Helper Functions #

def create_middle_window(samples, spb, i):
    begin_index = i * spb
    spb_window = samples[begin_index:begin_index + spb]
    assert len(spb_window) == spb
    middle_spb_window = spb_window
    if spb > 3:
        middle_spb_window = spb_window[spb/4 : 3 * spb/4]
    assert len(middle_spb_window) > 0
    return middle_spb_window

def find_databits(samples, preamble_start, spb, threshold_value):
    data_start = preamble_start + len(preamble) * spb
    data_samples = samples[data_start:]
    orig_data_len = len(data_samples) / spb
    out_samples = numpy.zeros(orig_data_len)
    for i in range(orig_data_len):
        middle_spb_window = create_middle_window(data_samples, spb, i)
        avg_value = numpy.mean(middle_spb_window)
        computed_value = 1 if avg_value > threshold_value else 0
        out_samples[i] = computed_value
    return out_samples

def valid_preamble(samples, spb, preamble_start, threshold_value):
    orig_preamble_length = len(preamble)
    ext_preamble_length = orig_preamble_length * spb
    ext_preamble = samples[preamble_start:preamble_start + ext_preamble_length]
    assert len(ext_preamble) == ext_preamble_length
    for i in range(orig_preamble_length):
        middle_spb_window = create_middle_window(ext_preamble, spb, i)  
        avg_value = numpy.mean(middle_spb_window)
        computed_value = 1 if avg_value > threshold_value else 0
        if (computed_value != preamble[i]):
            return False
    return True

def compute_threshold(samples, preamble_start, spb):
    orig_preamble_length = len(preamble)
    ext_preamble_length = orig_preamble_length * spb
    if len(samples) - preamble_start < ext_preamble_length:
        return None
    # Verify this is correct indexing
    ext_preamble = samples[preamble_start:preamble_start + ext_preamble_length]
    assert len(ext_preamble) == ext_preamble_length
    threshold_dict = {0:[], 1:[]}
    for i in range(orig_preamble_length):
        middle_spb_window = create_middle_window(ext_preamble, spb, i) 
        avg_value = numpy.mean(middle_spb_window)
        threshold_dict[preamble[i]].append(avg_value)
    zero_mean = numpy.mean(threshold_dict[0])
    one_mean = numpy.mean(threshold_dict[1])
    threshold_value = (float(zero_mean) + float(one_mean)) / 2
    return (zero_mean, one_mean, threshold_value)
